/**
	@file Union.c
	@brief Funciones del algoritmo unión
	@author Gonzalo Candia - Tamara Vivanco
*/

#include "List.h"
#include "Union.h"

/**
	@brief Función principal de Unión
	@param treeA información del k2-tree A
	@param treeB información del k2-tree B
*/
void Union(){
	struct timeval t_ini, t_fin;
	MyBM TA = NULL;
	MyBM LA = NULL;
	MyBM TB = NULL;
	MyBM LB = NULL;
	char nombreA[100],nombreB[100];

	printf("\nIngrese el nombre de matriz A:\n");
	scanf("%s",nombreA);
	printf("\nIngrese el nombre de matriz B:\n");
	scanf("%s",nombreB);

	INFO treeA = readInfo(nombreA);
	INFO treeB = readInfo(nombreB);

	TA = ReadBinary(nombreA,1,treeA.tamt);
	LA = ReadBinary(nombreA,0,treeA.taml);
	TB = ReadBinary(nombreB,1,treeB.tamt);
	LB = ReadBinary(nombreB,0,treeB.taml);

	ListRankUA = AssociateRankT(&TA);
	ListRankULA = AssociateRankL(&LA);
	ListRankUB = AssociateRankT(&TB);
	ListRankULB = AssociateRankL(&LB);
	gettimeofday(&t_ini, NULL);
	if (Uni(0,0,treeA,treeB) == 1){
		gettimeofday(&t_fin, NULL); 
		double secs = timeval_diff(&t_fin, &t_ini);
		printf("Existe union:\n");
		ViewList(C);
		printf("Tiempo de construcción: %.16g microsegs", secs * 1000000.0);
		fflush(stdin);
	}

	char archivo[100];
	strcpy(archivo,nombreA);

	FILE *fp;
	fp = fopen(strcat(archivo,"union.dat"),"w+");
	MyList aux = C;
	while(aux != NULL){
		fwrite(&(aux->var),sizeof(char),1,fp);
			aux = aux->next;
	}
	fclose(fp);
}

/**
	@brief Función para calcular la Unión
	@param pA dirección de memoria del valor de la posición del árbol A
	@param pB dirección de memoria del valor de la posición del árbol B
	@param treeA información del k2-tree A
	@param treeB información del k2-tree B
	@return 1, si existe unión
*/
int Uni(register int pA, register int pB, INFO treeA, INFO treeB){
	int *tuple;
	register int i,bA,bB,k2 = treeA.k * treeA.k;
	Queue Q = NULL;
	qNode L = NULL;
	tuple = (int*)malloc(sizeof(int)*3);
	Insert(&Q, &L, 0, 1, 1);
	while(Q != NULL){
		tuple = Delete(&Q);
		for (i = 0; i < k2; i++){
			bA = 0;
			bB = 0;
			if (tuple[1] == 1){
				bA = globalSearchUniA(pA,treeA.tamt);
				++pA;
			}
			if (tuple[2] == 1){
				bB = globalSearchUniB(pB,treeB.tamt);
				++pB;
			}
			if (bA == 1 || bB == 1){
				AddNodo(&C,&U,'1');
			}
			else{
				AddNodo(&C,&U,'0');
			}
			if ((tuple[0] < treeA.height-1) && (bA == 1 || bB == 1)){
				Insert(&Q, &L, (tuple[0] + 1), bA, bB);
			}
		}
	}
	return 1;
}

/**
	@brief Función para insertar en la cola
	@param *queue cola en la cual se insertarán los datos
	@param *last puntero al último nodo
	@param l valor del nivel a guardar
	@param a valor del primero entero a guardar
	@param b valor del segundo entero a guardar
*/
void Insert(Queue *queue, qNode *last, int l, int a, int b) {
	qNode new;
	Queue aux;

	new = (qNode)malloc(sizeof(nodeType));
	new->level = l;
	new->rA = a;
	new->rB = b;
	new->next = NULL; 
	if( *queue == NULL){
		*queue = new;
		*last = new;
	}else{
		aux = *last;
		aux->next = new;
		*last = new;
	}
}

/**
	@brief Función para extraer y eliminar elemento de cola
	@param *queue cola sobre la cuál se eliminará el elemento
	@return tupla extraída de cola
*/
int * Delete(Queue *queue) {
	qNode node;
	node = *queue;
	if(!node) return NULL;
	*queue = node->next;
	rt[0] = node->level;
	rt[1] = node->rA;
	rt[2] = node->rB;
	free(node);
	return rt;
}

int globalSearchUniA (int pos, int TAMT){
	if (pos < TAMT) return newRank(ListRankUA, pos);
	else return newRank(ListRankULA, pos-TAMT);
}

int globalSearchUniB (int pos, int TAMT){
	if (pos < TAMT) return newRank(ListRankUB, pos);
	else return newRank(ListRankULB, pos-TAMT);
}