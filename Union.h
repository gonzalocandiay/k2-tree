#include <stdio.h>
#include <stdlib.h>

#ifndef UNION_H
#define UNION_H

typedef struct _queue {
	int level;
	int rA;
	int rB;
	struct _queue *next;
} nodeType;
 
typedef nodeType *qNode;
typedef nodeType *Queue;
int rt[3];

Rank ListRankUA = NULL;
Rank ListRankULA = NULL;
Rank ListRankUB = NULL;
Rank ListRankULB = NULL;
MyList C = NULL;
MyList U = NULL;

void Insert(Queue *queue, qNode *last, int l, int a, int b);
int * Delete(Queue *queue);

int Uni(int pA, int pB, INFO treeA, INFO treeB);
int globalSearchUniA (int pos, int TAMT);
int globalSearchUniB (int pos, int TAMT);

#include "Union.c"
#endif