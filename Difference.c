/**
	@file Difference.c
	@brief Funciones del algoritmo diferencia
	@author Gonzalo Candia - Tamara Vivanco
*/

#include "Build.h"
#include "List.h"
#include "Complement.h"

/**
	@brief Función principal
*/
void Difference(){
	struct timeval t_ini, t_fin;
	MyBM TA = NULL;
	MyBM LA = NULL;
	MyBM TB = NULL;
	MyBM LB = NULL;
	char nombreA[100],nombreB[100];
	
	printf("\nIngrese el nombre de matriz A:\n");
	scanf("%s",nombreA);
	printf("\nIngrese el nombre de matriz B:\n");
	scanf("%s",nombreB);

	INFO treeA = readInfo(nombreA);
	INFO treeB = readInfo(nombreB);

	TA = ReadBinary(nombreA,1,treeA.tamt);
	LA = ReadBinary(nombreA,0,treeA.taml);
	TB = ReadBinary(nombreB,1,treeB.tamt);
	LB = ReadBinary(nombreB,0,treeB.taml);

	ListRankA = AssociateRankT(&TA);
	ListRankLA = AssociateRankL(&LA);
	ListRankB = AssociateRankT(&TB);
	ListRankLB = AssociateRankL(&LB);
	diff = CreateColumnOfC(treeA.pot,treeA.k);
	gettimeofday(&t_ini, NULL);
	if (Diff(0,0,0,treeA,treeB) == 1){
		gettimeofday(&t_fin, NULL); 
		double secs = timeval_diff(&t_fin, &t_ini);
		printf("Existe diferencia.\n");
		printf("Tiempo de construcción: %.16g microsegs", secs * 1000000.0);
	}
	else{
		printf("No existe diferencia.\n");
	}
	ViewListAdy(diff);
	ListAdy diffL = splitListAdy(diff);

	ListAdy aux=diff;
	while(aux!=NULL){
		if(aux->next!=NULL) aux->ultimo->next = aux->next->LA;
		aux=aux->next;
	}

	MyBM resT = CreateT(diff);
	MyBM resL = CreateL(diffL);

	char archivo[100];
	strcpy(archivo,nombreA);
	CreateBinary(resT,strcat(archivo,"diff"),1);
	strcpy(archivo,nombreA);
	CreateBinary(resL,strcat(archivo,"diff"),0);
}

/**
	@brief Función recursiva que permite determinar la diferencia
	@param l nivel del árbol a analizar
	@param pA dirección de memoria del valor de la posición del árbol A
	@param pB dirección de memoria del valor de la posición del árbol B
	@param treeA información del k2-tree A
	@param treeB información del k2-tree B
	@return 1, si existe diferencia, o 0 en caso contrario
*/
int Diff(register int l, register int pA, register int pB, INFO treeA, INFO treeB){

	register int sw = 0, i, k2 = treeA.k * treeA.k;
	int t[k2];
	for (i = 0; i < k2; i++){
		t[i] = 0; 
		if ((globalSearchDiffA(pA,treeA.tamt) == 1) && (globalSearchDiffB(pB,treeB.tamt) == 1)){
			if (l < treeA.height - 1){
				t[i] = Diff((l + 1), Child(pA,k2,ListRankA), Child(pB,k2,ListRankB), treeA, treeB);
			}
			else t[i] = 0;
		}
		else if ((globalSearchDiffA(pA,treeA.tamt) == 1) && (globalSearchDiffB(pB,treeB.tamt) == 0)){
			if (l < treeA.height - 1){
				t[i] = Copy((l + 1),Child(pA,k2,ListRankA),treeA);
			}
			else t[i] = 1;
		}
		sw |= t[i];
		++pA;
		++pB;
	}
	if (sw == 1)
		for(i = 0; i < k2; i++){
			if(t[i] == 1) catLevel(diff,l,'1');
			if(t[i] == 0) catLevel(diff,l,'0');
		}
	return sw;
}

/**
	@brief Función para copiar lo valores del árbol A en la estructura final
	@param level nivel del árbol
	@param pA posición del árbol A
	@param tree información del árbol A
	@return 1, si se realizó la operación
*/
int Copy(register int level, register int pA, INFO tree){
	register int i,j,aux,ret,k2 = tree.k * tree.k;
	if (level <= (tree.height-1)){
		for (i = 0; i < k2; i++){
			aux = pA + i;
			if (globalSearchDiffA(aux,tree.tamt) == 1) catLevel(diff,level,'1');
			else catLevel(diff,level,'0');
			if ((level < tree.height) && (globalSearchDiffA(aux,tree.tamt) == 1))
				ret=Copy(level + 1, Child(aux,k2,ListRankA), tree);
		}
		return ret;
	}
	else return 1;
}

int globalSearchDiffA (int pos, int TAMT){
	if (pos < TAMT) return newRank(ListRankA, pos);
	else return newRank(ListRankLA, pos-TAMT);
}

int globalSearchDiffB (int pos, int TAMT){
	if (pos < TAMT) return newRank(ListRankB, pos);
	else return newRank(ListRankLB, pos-TAMT);
}
