#include<stdio.h>

#ifndef BIT_H
#define BIT_H

//defino el tama�o del int
#define INT_32 32

/* 32 bit vector defines
*  se pueden definir los 32, pero aun no se requieren
#define BM0        (1 <<  0)
#define BM1        (1 <<  1)
#define BM2        (1 <<  2)
#define BM3        (1 <<  3)
#define BM4        (1 <<  4)
#define BM5        (1 <<  5)
#define BM6        (1 <<  6)
#define BM7        (1 <<  7)
*/

/* IS_SET comprueba que el bitvector flag contiene el bit encendido o no utilizando el operador bit a bit AND */
#define IS_SET(flag, bit)        ((flag) & (bit))

/* SET_BIT enciende el bit en el bitvector var utilizando el operador de asignaci�n de bits OR */
#define SET_BIT(var, bit)        ((var) |= (bit))

/* REMOVE_BIT apaga el bit en el bitvector var utilizando el operador de asignaci�n AND y el NOT */
#define REMOVE_BIT(var, bit)     ((var) &= ~(bit))

/* TOGGLE_BIT conmuta entre encendido y apagado el bit en el bitvector var utilizando el operador de asignaci�n XOR */
#define TOGGLE_BIT(var, bit)	 ((var) ^= (bit))

/* visualiza cada bit de un entero */
void ViewBinario(int n);
void Reset(int n);
int Ones(int x);


#include"BIT.c"
#endif /*BIT_H*/
