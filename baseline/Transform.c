#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#define W 8

int LeerMatriz(char * filename,char * bm);
int getN(char *filename);
void getPositions(char *bm, int n, int unos, int * pos);
void convert(char *bm, int unos, int * pos);
void printbitMap(char *bm, int n);

int main(int argc, char *argv[])
{
	if(argc < 3){
		printf("%s\n", "uso: Transform txt_origen bin_salida");
		exit(1);
	}
	long i;
	int n = getN(argv[1]);
	printf("N = %d\n",n);
	char * bmIn = (char *)malloc(n*n*sizeof(char));
	int unos = LeerMatriz(argv[1],bmIn);
	printf("unos = %d\n", unos);
	char * bmOut = malloc((long)n/W * (long) n);

	for(i = 0; i < ((long)n/W) * (long) n; i++) bmOut[i]='\0';
	int * pos = malloc(sizeof(int)*unos);

	getPositions(bmIn,n,unos,pos);
	convert(bmOut,unos,pos);
	FILE *out=fopen(argv[2],"w+");
	char *ptr = &n;
 	fwrite(ptr,4,1,out);
	fwrite(bmOut,(long)n/W * (long) n,1,out);
	fclose(out);
	printbitMap(bmOut,n);

	return 0;
}

int LeerMatriz(char * filename,char * bm){
	int n,i,j,k=0,unos = 0,l;
	FILE *f;
	f=fopen(filename,"r");
	fscanf(f,"%d",&n);
	char fila[n];
	for(i=0;i<n;i++){
		fscanf(f,"%s",fila);
		for (j = 0; j < n; j++,k++){
			if(fila[j] == '1') unos++;
			bm[k] = fila[j];
		}
	}
	fclose(f);
	return unos;
}

int getN(char *filename){
	int n; char * str;
	str = malloc(sizeof(char) * 3);
	FILE *f;
	f = fopen(filename,"r");
	fscanf(f,"%s",str);
	n=atoi(str);
	fclose(f);
	return n;
}

void getPositions(char *bm, int n, int unos, int * pos){
	int i,k = 0;
	for (i = 0; i < n*n; i++){
		if(bm[i] == '1') {
			pos[k] = i;
			k++;
		}
	}
}

void convert(char *bm, int unos, int * pos) {
	int i = 0;
	while (i < unos){
		long r = pos[i];
		long p = r/W;
		unsigned char bit = r-p*W+1;
		unsigned char msk = (0x01)<<(W-bit);
		if ((bm[p] & msk) == 0){
			bm[p] = bm[p] | msk;
			i++;
		}
		//printf("i = %d, unos = %d\n",i,unos );
	}
}

void printbitMap(char *bm, int n) {
 int j,k,i;
 for(i=0; i <n;i++) {
  for (j=0;j < n/W;j++) {
    unsigned char msk1=0x80;
      for (k = 0; k < W; k++) {
       printf("%d", ((bm[j+i*n/W] & msk1) !=0) ? 1 :0);
       msk1 = msk1 >> 1;
      }
    }
   printf("\n");
 }
 }
