#include<stdio.h>
#include <stdlib.h>
#include <math.h>
#include<time.h>
#define W 8 

//----------------------------------------------------------------------------------------
//Este programa genera un archivo (dado como parametro) con el siguiente formato
//b0b1b2b3b4b5....bm, con b0...b3 4 bytes para almacenar k(k es un entero potencia de 2)
//el byte mas significativo es b0 y el menos significativo es b3.
// De b4...bm van los (k^2*k^2)/8 bytes del bitmap.
//---------------------------------------------------------------------------------------

void main(int argc, char **argv){
 if(argc < 4) { 
  printf("%s\n","uso: createBitMapDiskM n unos file");
  exit(1);
 }
  int n = atoi(argv[1]);  
  int unos = atoi(argv[2]); // Cuantos 1's quiero en el archivo 
  if((n%W) != 0) { 
   printf("%s \n", "n debe ser multiplo de 8");
   exit(1);
  }
 FILE *out=fopen(argv[3], "r");
 if(out != NULL) { printf("!Cuidado el archivo ya existe¡ \n"); exit(1); }
   else { out=fopen(argv[3], "w+");}

 char * bm = malloc((long)n/W * (long) n);
 long i = 0; 
 for(i = 0; i < ((long)n/W) * (long) n; i++) bm[i]='\0';
 setBitsUniform(bm,n, unos);
// printbitMap(bm, n);
// genGraph(bm, n);


// Se escribe bitmap en archivo 
 char *ptr = &n;
 fwrite(ptr,4,1,out);
 fwrite(bm,(long)n/W * (long) n,1,out);
 fclose(out);

//printf("%s\n","Terminado");
} 
//-----------funciones
//---Se ponen a 1 los bits que se generan al azar con rand()
//---cada vez que se ejecuta este programa
void setBitsUniform(char *bm, int n, int unos) { 

srand(time(NULL)); 
int i =0;
while (i < unos){ 
   long r = rand()%((n * n)); 
   long p = r/W;
   unsigned char bit = r-p*W+1;
   unsigned char msk = (0x01)<<(W-bit);
    if ((bm[p] & msk) == 0)  {
       bm[p] = bm[p] | msk; 
       i++;
    } 
  }
 }

void printbitMap(char *bm, int n) { 
 int j,k,i; 
 for(i=0; i <n;i++) {
  for (j=0;j < n/W;j++) {
    unsigned char msk1=0x80;
      for (k = 0; k < W; k++) {
       printf("%d", ((bm[j+i*n/W] & msk1) !=0) ? 1 :0); 
       msk1 = msk1 >> 1; 
      } 
    } 
   printf("\n");
 } 
 }

// genera lista de adyacencia 
 void genGraph(char *bm, int n){
 int i,j,k; 
 for(i=0;i <n;i++) {
  for (j=0;j < n/W;j++) {
    unsigned char msk1=0x80;
      for (k = 0; k < W; k++) {
       if ((bm[j+i*n/W] & msk1) !=0) printf("%d ", (j*W+k));
       msk1 = msk1 >> 1; 
      } 
    } 
   printf("\n");
  }
 }

