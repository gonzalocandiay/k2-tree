#include<stdio.h>
#include <stdlib.h>
#include <math.h>
#include<time.h>

double timeval_diff(struct timeval *a, struct timeval *b);

void main(int argc, char **argv){
  struct timeval t_ini, t_fin;
  double secs;
  gettimeofday(&t_ini, NULL);
  if(argc != 6) { 
    printf("%s\n","uso: operations file1 file2 file3 op buffersize");
    printf("%s\n","op: (1: union, 2: Intersection y 3: Difference.  buffersize in bytes");
    exit(1);
  }

  FILE *in1=fopen(argv[1], "r");
  FILE *in2=fopen(argv[2], "r");
  FILE *out=fopen(argv[3], "r");

  int op = atoi(argv[4]); //Cual es la operacion quen se desea ejecutar.
  int bz = atoi(argv[5]); // Tamanho del buffer

  if(op > 3 || op < 1) { 
     printf("Operador entre 1 y 3\n");
     exit(1); 
  }
     
  if(in1 == NULL || in2 ==NULL) { 
     printf("Uno o ambos archivos  %s y  %s no existen \n", argv[1], argv[2] ); 
     exit(1); 
  }

  if(out !=NULL) { 
     printf("El archivo %s ya existe!!! \n",  argv[3]); 
     exit(1); 
  } else out=fopen(argv[3], "w");

  // Obtiene los tamanhos (n) de llos bitmaps
  int n1 = getN(in1); 
  int n2 = getN(in2);
  if(n1 != n2) { 
     printf("%s\n", "los archivos de entrada tienen diferentes dimensiones" ); 
     exit(1); 
  }
  // Ejecuta el operador
  binOperator(in1, in2, out, bz, n1, op);
  gettimeofday(&t_fin, NULL); 
  secs = timeval_diff(&t_fin, &t_ini);
  printf("Tiempo de construcción: %.16g ms", secs * 1000000.0);
  //--cerrar los archivos y terminar
  fclose(out);
  fclose(in1);
  fclose(in2);
} // fin main

//-----------------------------------------------------
//-Operadores Binarios: Union, Intersection and difference.
 void  binOperator(FILE *in1, FILE *in2, FILE *out, int buffersize, int n,int op) { 
// Se define un buffer para cada archivo.
// Se usa el buf1 para guardas los resultados de la operación y grabar en out.
 char buf1[buffersize], buf2[buffersize];
 int bl1, bl2,i;
 char *ptr = &n;
// graba en archivo de salida el valor de n (4 bytes)
 putc(*ptr,out); putc(*(ptr+1),out); putc(*(ptr+2),out); putc(*(ptr+3),out);
// se desplaza 4 bytes (in1 e in2) desde el principio para empezar a leer el bitmap
 fseek(in1,4,0); fseek(in2,4,0);
 do {
     bl1 = fread(buf1,1,buffersize,in1);
     bl2 = fread(buf2,1,buffersize,in2);
     switch (op) { 
     case 1: //union
        for(i=0; i < bl1; i++) 
        buf1[i] = buf1[i] | buf2[i];
        break;
     case 2: //intersection
        for(i=0; i < bl1; i++) 
        buf1[i] = buf1[i] &  buf2[i];
        break;
     case 3:// difference
        for(i=0; i < bl1; i++) 
        buf1[i] = buf1[i]  &  ~buf2[i];
     } 
     //---se escriben en el archivo de salida
     fwrite(buf1,bl1,1,out);
 } while ( bl1>= buffersize);
} // 

//Obtiene  n desde el file f
int getN(FILE *f) { 
  char buf[4];
  rewind(f);
  int rb = fread(buf,1,4,f);
  rewind(f);
  int *n = &buf;
  return *n;
}

double timeval_diff(struct timeval *a, struct timeval *b){
  return
  (double)(a->tv_sec + (double)a->tv_usec/1000000) -
  (double)(b->tv_sec + (double)b->tv_usec/1000000);
}