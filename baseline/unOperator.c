#include<stdio.h>
#include <stdlib.h>
#include <math.h>
#include<time.h>
double timeval_diff(struct timeval *a, struct timeval *b);

void main(int argc, char **argv){
  struct timeval t_ini, t_fin;
  double secs;
  gettimeofday(&t_ini, NULL);
  if(argc != 4) { 
    printf("%s\n","uso: operations file1  file2 buffersize");
    exit(1);
  }

  FILE *in1=fopen(argv[1], "r");
  FILE *out=fopen(argv[2], "r");
  int bz = atoi(argv[3]); // Tamanho del buffer
     
  if(in1 == NULL) { 
     printf("Uno o ambos archivos  %s y  %s no existen \n", argv[1], argv[2] ); 
     exit(1); 
  }

  if(out !=NULL) { 
     printf("El archivo %s ya existe!!! \n",  argv[3]); 
     exit(1); 
  } else out=fopen(argv[2], "w");

  // Obtiene los tamanhos (n) de llos bitmaps
  int n1 = getN(in1); 
  // Ejecuta el operador

  complement(in1, out, bz, n1);
  gettimeofday(&t_fin, NULL); 
  secs = timeval_diff(&t_fin, &t_ini);
  printf("Tiempo de construcción: %.16g ms", secs * 1000000.0);
  //--cerrar los archivos y terminar
  fclose(out);
  fclose(in1);
} // fin main

//-----------------------------------------------------
//-Operadores Binarios: Union, Intersection and difference.
 void  complement(FILE *in1, FILE *out, int buffersize, int n) { 
// Se define un buffer para cada archivo.
 char buf1[buffersize];
 int bl1, i;
 char *ptr = &n;
// graba en archivo de salida el valor de n (4 bytes)
 putc(*ptr,out); putc(*(ptr+1),out); putc(*(ptr+2),out); putc(*(ptr+3),out);
// se desplaza 4 bytes (in1 e in2) desde el principio para empezar a leer el bitmap
 fseek(in1,4,0); 
 do {
     bl1 = fread(buf1,1,buffersize,in1);
     for(i=0; i < bl1; i++) 
     buf1[i] = ~buf1[i];
     fwrite(buf1,bl1,1,out);
 } while ( bl1>= buffersize);
} // 

//Obtiene  n desde el file f
int getN(FILE *f) { 
  char buf[4];
  rewind(f);
  int rb = fread(buf,1,4,f);
  rewind(f);
  int *n = &buf;
  return *n;
}

double timeval_diff(struct timeval *a, struct timeval *b){
  return
  (double)(a->tv_sec + (double)a->tv_usec/1000000) -
  (double)(b->tv_sec + (double)b->tv_usec/1000000);
}