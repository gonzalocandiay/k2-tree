#include<stdio.h>
#include<stdlib.h>

#ifndef __LIST_H
#define __LIST_H
//estructura L o T
typedef struct nodobm{
	int var;
	struct nodobm *next;
}bitmap;
typedef bitmap *MyBM;

//estructura arbol bitmap o lista de adyacencia
typedef struct nodo{
	char var;
	struct nodo *next;
}list;

typedef list *MyList;
typedef list *Nodo;

typedef struct supernodo{
	MyList LA,ultimo;
	int nivel;
	struct supernodo *next;
}superlist;

typedef superlist *ListAdy;

//lista para almacenar los niveles de T
ListAdy lt=NULL;
//lista para almacenar L
ListAdy ll=NULL;
//ListAdy comp=NULL;

MyBM TL=NULL; //T y L concatenado

//agregar entero a la lista L o T
//l es la lista y u es puntero al ultimo
MyBM AddNodoLT(MyBM *l,MyBM *u, int c);
void ViewBM(MyBM l);

//funciones para lista de adyacencia y/o arbol de bitmap (funcion recursiva)
void AddNodo(MyList *l,MyList *u, char c);
void ViewList(MyList l);
MyBM Cat(MyBM *T, MyBM *L);
void ViewListAdy(ListAdy list);

#include"List.c"
#endif /*__LIST_H*/
