/**
	@file List.c
	@brief Archivo con funciones de manipulación de listas.
	@author Gonzalo Candia - Tamara Vivanco
*/

#include "List.h"
#include "BIT.h"

/**
	@brief Funciones para generar lista L o T
	@param *l cabecera de la lista a añadir elemento
	@param *u dirección de memoria del último elemento
	@param c elemento a añadir a lista
	@return dirección de memoria del último elemento
*/
MyBM AddNodoLT(MyBM *l,MyBM *u, int c){
	MyBM lista, nuevo;
	nuevo = (MyBM)malloc(sizeof(bitmap));
	nuevo->var=c;
	nuevo->next=NULL;
	if(*l==NULL){
		*l=nuevo;
		*u=*l;
	} 
	else{
		lista=*u;
		lista->next=nuevo;
		*u=nuevo;
	}
	return *u;
}

/**
	@brief Función para mostrar bitmap
	@param l bitmap a mostrar
*/
void ViewBM(MyBM l){
	MyBM aux;
	aux = l;
	register int i=0;
	while (aux != NULL){
		printf("%d=> ",i);
		ViewBinario(aux->var);	
		printf("\n");
		aux = aux->next;
		i++;
	}
}

/**
	@brief Función para agregar nodo a la lista de adyacencia
	@param *l cabecera de lista de adyacencia
	@param *u dirección de memoria del último elemento de la lista de adyacencia
	@param c carácter a añadir a la lista
*/
void AddNodo(MyList *l,MyList *u, char c){
	Nodo lista, nuevo;
	nuevo = (Nodo)malloc(sizeof(list));
	nuevo->var=c;
	nuevo->next=NULL;
	
	if(*l==NULL){
		*l=nuevo;
		*u=*l;
	} 
	else{
		lista=*u;
		lista->next=nuevo;
		*u=nuevo;
	}
}

/**
	@brief Función para mostrar lista de adyacencia
	@param list lista a mostrar
*/
void ViewListAdy(ListAdy list){
	ListAdy lista=list;
	while(lista!=NULL){
		printf("Nivel %d:\n",lista->nivel);
		MyList l = lista->LA;
		ViewList(l);
		lista=lista->next;
		printf("\n\n");
	}	
}

/**
	@brief Función para mostrar lista
	@param l lista a mostrar
*/
void ViewList(MyList l){
	Nodo aux;
	aux = l;
	while (aux != NULL){
		printf("%c", aux->var);
		aux = aux->next;
	}
}

/**
	@brief Función para dividir lista de adyacencia
	@param res lista de adyacencia a dividir
*/
ListAdy splitListAdy(ListAdy res){
	ListAdy ll, aux = res, aux2;

	while(aux!= NULL){
		if(aux->next->next == NULL){ 
			aux2 = aux->next;
			ll = aux2;
			aux->next = NULL;
			break;
		}
		aux = aux->next;
	}

	return ll;
}