/**
	@file Rank.c
	@brief Funciones relativas a Rank.
	@author Gonzalo Candia - Tamara Vivanco
*/


#include "Rank.h"
#define FACTOR 20

//funciones
Rank AddNodoToRank(Rank *l,Rank *u,MyBM *bm,unsigned int c){
	Rank lista, nuevo;
	nuevo = (Rank)malloc(sizeof(R));
	nuevo->cantidad=c;
	nuevo->nodo=*bm;
	nuevo->next=NULL;
	if(*l==NULL){
		*l=nuevo;
		*u=*l;
	} 
	else{
		lista=*u;
		lista->next=nuevo;
		*u=nuevo;
	}
	return *u;
}

void ViewRank(Rank l){
	Rank aux = l;
	while (aux != NULL){
		printf("Count Rank %d \n", aux->cantidad); 	
		aux = aux->next;
	}
}

int ApplyRank(Rank l, int n){
	Rank aux = l,ult = l;
	register int pos = (FACTOR*32),i,c = 0,val = (FACTOR*32);
	
	if(n==0){
		if(IS_SET(l->nodo->var,(1 <<  31)) )return 1;
		return 0;
	}
	
	while (aux != NULL && pos <= n){
		pos+=val;// 20 enteros por 32 bits
		aux = aux->next;
		ult = aux; 
		c = ult->cantidad;
	}
	pos-=val;

	MyBM aux2;
	if(n < val) aux2=ult->nodo;
	else aux2=ult->nodo->next;
		
	while(aux2!=NULL && pos<=n){
		for(i=31;i>=0 && pos<=n;i--){
			if (IS_SET(aux2->var,(1 <<  i)) ) c++;		
				
			if(pos==n)return c;
			++pos;
		}
		aux2=aux2->next;
	}
}

int RankSetL(Rank l, int n){
	Rank aux=l,ult;
	aux = l;
	int i,pos=0;
	ult=l;
	while (aux->next != NULL && aux->next->cantidad <= n){
		ult=aux;
		 pos=ult->cantidad; 	
		aux = aux->next;
		//puts("paso\n");
	}
	//printf("la pos es %d \n", pos);
	
		MyBM aux2=ult->nodo;
		while(aux2!=NULL && pos<=n){
			if(pos+32 <= n){
				//puts("<=n\n");
				aux2=aux2->next;
				pos+=32;	
			}
			else{
				for(i=31;i>=0 ;i--){
					if(pos==n){
						//printf("entro en %d ",pos);
						//ViewBinario(aux2->var);
						if (IS_SET(aux2->var,(1 <<  i)) ) return 1;
						return 0;
					}
					pos++;
				}
			}
		}
}



int RankSetT(Rank l, int n){
	Rank aux=l,ult;
	aux = l;
	int i,pos=0;
	ult=l;
	while (aux->next != NULL && (pos+(32*20)) <= n){
		ult=aux;
		 pos+=(20*32); 	
		aux = aux->next;
		//puts("paso\n");
	}
	//printf("la pos es %d \n", pos);
	
		MyBM aux2=ult->nodo;
		while(aux2!=NULL && pos<=n){
			if(pos+32 <= n){
				//puts("<=n\n");
				aux2=aux2->next;
				pos+=32;	
			}
			else{
				for(i=31;i>=0 ;i--){
					if(pos==n){
						//printf("entro en %d ",pos);
						//ViewBinario(aux2->var);
						if (IS_SET(aux2->var,(1 <<  i)) ) return 1;
						return 0;
					}
					pos++;
				}
			}
		}
}

Rank AssociateRankT(MyBM *bm){
	int nodes = 0,count = 0,ones;
	MyBM aux = *bm;
	Rank last=NULL,head=NULL;
	while(aux!=NULL){
		count++;
		nodes++;
		ones = (ones + Ones(aux->var));
		if (nodes == 1){
			last = AddNodoToRank(&head,&last,&aux,ones);
		}
		if (count == FACTOR){
			last = AddNodoToRank(&head,&last,&aux,ones);
			count = 0;
		}
		aux=aux->next;
	}
	return head;
}

Rank AssociateRankL(MyBM *bm){
	int nodes = 0,count = 0;
	MyBM aux = *bm;
	Rank last,head=NULL;
	while(aux!=NULL){
		count++;
		nodes++;
		if (nodes == 1){
			last = AddNodoToRank(&head,&last,&aux,0);
		}
		if (count == FACTOR){
			last = AddNodoToRank(&head,&last,&aux,nodes*32);
			count = 0;
		}
		aux=aux->next;
	}

	return head;
}

int newRank(Rank l, int n){
	Rank aux = l,ult = l;
	register int val = FACTOR*32;
	register int pos = val,i;
	
	if(n == 0){
		if(IS_SET(l->nodo->var,(1 <<  31)) ) return 1;
		return 0;
	}
	
	while (aux != NULL && pos <= n){
		pos+=(val);// 20 enteros por 32 bits 	
		aux = aux->next;
		ult=aux;
	}
	pos-=(val);
	
	// ->next para pasar a contar desde la sig posicion
	MyBM aux2;
	if(n<val) aux2=ult->nodo;
	else aux2=ult->nodo->next;
		
	while(aux2!=NULL && pos<=n){
		for(i=31;i>=0 && pos<=n;i--){
			if(pos == n){
				if (IS_SET(aux2->var,(1 <<  i)) ) return 1;
				return 0;
			}
			++pos;
		}
		aux2=aux2->next;
	} 
}