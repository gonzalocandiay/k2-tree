/**
	@file Complement.c
	@brief Funciones del algoritmo de intersección
	@author Gonzalo Candia - Tamara Vivanco
*/

#include "Build.h"
#include "List.h"
#include "Complement.h"
#include "Intersection.h"

/**
	@brief Función principal de intersección
*/
void Intersection(){
	struct timeval t_ini, t_fin;
	MyBM TA = NULL;
	MyBM LA = NULL;
	MyBM TB = NULL;
	MyBM LB = NULL;
	char nombreA[100],nombreB[100];
	printf("==== INTERSECCION ====\n");
	printf("\nIngrese el nombre de matriz A:\n");
	scanf("%s",nombreA);
	printf("\nIngrese el nombre de matriz B:\n");
	scanf("%s",nombreB);

	INFO treeA = readInfo(nombreA);
	INFO treeB = readInfo(nombreB);

	TA = ReadBinary(nombreA,1,treeA.tamt);
	LA = ReadBinary(nombreA,0,treeA.taml);
	TB = ReadBinary(nombreB,1,treeB.tamt);
	LB = ReadBinary(nombreB,0,treeB.taml);

	ListRankIA = AssociateRankT(&TA);
	ListRankILA = AssociateRankL(&LA);
	ListRankIB = AssociateRankT(&TB);
	ListRankILB = AssociateRankL(&LB);
	inter = CreateColumnOfC(treeA.pot,treeA.k);
	
	gettimeofday(&t_ini, NULL);
	if (Inter(0,0,0,treeA,treeB) == 1){
		gettimeofday(&t_fin, NULL); 
		printf("Existe interseccion.\n");
		double secs = timeval_diff(&t_fin, &t_ini);
		printf("Tiempo de construcción: %.16g microsegs", secs * 1000000.0);
	}
	else{
		printf("Intersección vacia.\n");
	}

	ViewListAdy(inter);

	ListAdy interL = splitListAdy(inter);

	ListAdy aux=inter;
	while(aux!=NULL){
		if(aux->next!=NULL) aux->ultimo->next = aux->next->LA;
		aux=aux->next;
	}

	MyBM resT = CreateT(inter);
	MyBM resL = CreateL(interL);

	char archivo[100];
	strcpy(archivo,nombreA);
	CreateBinary(resT,strcat(archivo,"inter"),1);
	strcpy(archivo,nombreA);
	CreateBinary(resL,strcat(archivo,"inter"),0);
}

/**
	@brief Función recursiva que permite determinar la intersección
	@param l nivel del árbol a analizar
	@param pA dirección de memoria del valor de la posición del árbol A
	@param pB dirección de memoria del valor de la posición del árbol B
	@param treeA información del k2-tree A
	@param treeB información del k2-tree B
	@return 1, si existe intersección, o 0 en caso contrario
*/
int Inter(int l, int pA, int pB, INFO treeA, INFO treeB){
	register int sw = 0, i, k2 = treeA.k * treeA.k;
	int t[k2];
	for (i = 0; i < k2; i++){
		t[i] = 0;
		if ((globalSearchInterA(pA,treeA.tamt) == 1) && (globalSearchInterB(pB,treeB.tamt) == 1)){
			if (l < treeA.height - 1){
				t[i] = Inter((l + 1), Child(pA,k2,ListRankIA), Child(pB,k2,ListRankIB), treeA, treeB);
			}
			else t[i] = 1;
		}
		sw |= t[i];
		++pA;
		++pB;
	}
	if (sw == 1)
		for(i = 0; i < k2; i++){
			if(t[i] == 1) catLevel(inter,l,'1');
			if(t[i] == 0) catLevel(inter,l,'0');
		}
	return sw;
}

int globalSearchInterA (int pos, int TAMT){
	if (pos < TAMT) return newRank(ListRankIA, pos);
	else return newRank(ListRankILA, pos-TAMT);
}

int globalSearchInterB (int pos, int TAMT){
	if (pos < TAMT) return newRank(ListRankIB, pos);
	else return newRank(ListRankILB, pos-TAMT);
}