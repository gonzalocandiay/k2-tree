README

1. Compilación

Compilar en Linux usando gcc, añadiendo la opción -lm (link a librería math).
Ejecutar desde terminal, utilizando ./nombreejecutable

2. Uso
2.1. Creación k2-tree

Elegir opción 1) Construir k2-tree.
Se solicitará nombre del archivo binario de entrada para construir. (*)
El archivo binario de origen debe estar en la misma carpeta donde se encuentra el programa principal.
Genera 3 archivos: .il (información de árbol), .dat (T) y .dat (L).

2.2. Visualización en consola

Elegir opción 2) Ver Bitmap
Ingresar nombre de archivo a visualizar (tiene que haber sido creado previamente).

2.3. Operacion unaria Complemento

Elegir opción 3) Complemento.
Ingresar nombre de archivo que contiene el k2-tree creado anteriormente.
Se generan los correspondientes archivos T y L. 

2.4. Operaciones binarias
2.4.1. Diferencia

Elegir opción 4) Diferencia.
Ingresar nombres de archivos de entrada de k2-tree creados anteriormente.
Se generan los archivos resultantes T y L.

2.4.2. Intersección

Elegir opción 5) Intersección.
Ingresar nombres de archivos de entrada de k2-tree creados anteriormente.
Se generan los archivos resultantes T y L.

2.4.3. Unión
Elegir opción 6) Unión.
Ingresar nombres de archivos de entrada de k2-tree creados anteriormente.
Se generan los archivos resultantes T y L.

ANEXO

* Creación de archivos binarios de entrada.

