/**
	@file BIT.c
	@brief Funciones relativas a manejo de bits.
	@author Gonzalo Candia - Tamara Vivanco
*/
#include "BIT.h"

/**
	@brief Función para visualizar cada bit de un entero
	@param n
	@return
*/
void ViewBinario(int n){
	int i,j=0;
	for(i=31;i>=0;i--){
		if((i-1)%10==0)printf(" ");
		if (IS_SET(n,(1 <<  i)) ){
			printf("1");
			j++;
		}
		else printf("0");
	}
	//printf("  c=> %d ",j);	imprime la cantidad de 1 en cada entero
}

/**
	@brief
	@param x
	@return
*/
int Ones(int x){
	int i,count=0;
	for (i = 31; i >= 0; i--){
		if (IS_SET(x,(1 <<  i))){
			count++;
		}
	}
	return count;
}

/**
	@brief
	@param n
*/
void Reset(int n){
	int i;
	for(i=31;i>=0;i--){
		if (IS_SET(n,(1 <<  i)) ){
			REMOVE_BIT(n,i);
		}
	}
}
