#include <stdio.h>
#include <stdlib.h>

#ifndef DIFFERENCE_H
#define DIFFERENCE_H

ListAdy diff = NULL;
Rank ListRankA = NULL;
Rank ListRankLA = NULL;
Rank ListRankB = NULL;
Rank ListRankLB = NULL;

int Diff(register int l, register int pA, register int pB, INFO treeA, INFO treeB);
int Copy(register int level, register int pA, INFO tree);
int globalSearchDiffA (int pos, int TAMT);
int globalSearchDiffB (int pos, int TAMT);

#include "Difference.c"
#endif