#include<stdio.h>
#include<stdlib.h>

#include"BIT.h"
#include"List.h"

#ifndef __RANK_H
#define __RANK_H

//estructura
typedef struct nodoR{
	MyBM nodo;
	//MyBM ultimo;
	//unsigned 
	int cantidad; 
	struct nodoR *next;
}R;

typedef R *Rank;

//Rank ListRank=NULL; //LISTA RANK T
//Rank ListRankL=NULL; //LISTA RANK L
//Rank UR=NULL; // puntero al ultimo


//funciones para agregar, visualizar y contar
Rank AddNodoToRank(Rank *l,Rank *u,MyBM *bm,unsigned int c); // agrega nodo a  lista rank!
void ViewRank(Rank l); 			// ver rank como lista
int ApplyRank(Rank l, int n); // RANK DE T, cantidad de 1 hasta n
int RankSetL(Rank l, int n); // indica si en la posicion n es 0 o 1, en BIT MAP L
int RankSetT(Rank l, int n); // indica si en la posicion n es 0 o 1, en BIT MAP T
Rank AssociateRankT(MyBM *bm);
Rank AssociateRankL(MyBM *bm);
int newRank(Rank l, int n);

#include"Rank.c"
#endif /*__RANK_H*/
