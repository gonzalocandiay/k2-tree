/**
	@file Build.c
	@brief Archivo con funciones de construcción de k2-tree
	@author Gonzalo Candia - Tamara Vivanco
*/

#include "Build.h"
#include "BIT.h"
#include "List.h"
#include "Rank.h"

/**
	@brief Función principal de construcción de k2-tree
	@return información del árbol creado en un estructura tipo INFO
*/
void Build(){
	int totalR = 0,totalRL = 0,totalT = 0,i,j;
	char nombre[100];
	double secs;
	struct timeval t_ini, t_fin;
	MyBM L=NULL;// BIT MAP L
	MyBM T=NULL;// BIT MAP T
	MyBM U=NULL;// puntero al ultimo elemento de L y T
	Rank ListRank=NULL; //LISTA RANK T
	Rank ListRankL=NULL; //LISTA RANK L
	Rank UR=NULL; // puntero al ultimo

	INFO info;
	info.n=0;
	info.k=0;
	info.tamt=0;
	info.taml=0;
	info.pot=0;
	info.height=0;
	
	ll=(ListAdy)malloc(sizeof(superlist));
	ll->nivel=0;
	ll->next=NULL;
	ll->LA=NULL;
	ll->ultimo=NULL;
	
	FILE *f;
	do{
		printf("\nIngrese el nombre matriz de entrada:\n");
		scanf("%s",nombre);
		fflush(stdin);
		printf("Abriendo archivo %s\n",nombre);
		f=fopen(nombre,"r");
		if(f == NULL) printf("No se encontró el archivo.\n");
	}while(f == NULL);
	
	info.n = getN(f);
	fseek(f,4,0);

	char **bm = (char **)malloc(info.n*sizeof(char*)); 
	for (i = 0; i < info.n; i++) bm[i] = (char*)malloc(info.n*sizeof(char)); 	
	
	if (info.n==0){
		printf("No existe matriz en el archivo\n");
	}
	else{
		do{
			if(info.k >= info.n)printf("\nIngrese un valor para K (K debe ser menor al tamaño de la matriz (%d):\n", info.n);
			if(info.k <= 1)printf("\nIngrese un valor para K (K debe ser mayor a 1):\n");	
			scanf("%d",&info.k); 
		}while(info.k >= info.n || info.k <= 1);
		info.height = (int)(log10(info.n)/log10(info.k));
		info.pot = ProxPotencia(info.n,info.k);
		gettimeofday(&t_ini, NULL);
		
		char * bmap = malloc((long)info.n/8 * (long)info.n);
		for(i = 0; i < ((long) info.n/8) * (long) info.n; i++) bmap[i]='\0';
		fread(bmap,(long)info.n/8 * (long)info.n,1,f);
		readFile(bm,bmap,info.n);
		fclose(f);

		puts("Creando Bitmap.\n");

		lt=CreateColumnOf(info.pot,info.k); 
		char a=BitMap(info.pot,info.n,0,0,0,bm,info.k);
		free(bm);

		ListAdy aux=lt;
		while(aux!=NULL){
			if(aux->next!=NULL)aux->ultimo->next=aux->next->LA;
			aux=aux->next;
		}

		puts("Pasando T a Bits.\n");
		int ct=0,cuno=0;
		
		MyList aux2=lt->LA;
		MyList inicio,temporal;
		while(aux2!=NULL){
			inicio=aux2;
			int numero=0;//todos los bit en 0
			for(i=1; i<=INT_32 && aux2!=NULL ;i++){    //paso 1=> cada 32 nodos creo un entero con sus bits "seteados"					
				if(aux2->var=='1'){
					SET_BIT(numero, (1 << (INT_32-i) ));
					cuno++;
				}
				info.tamt++;
				aux2=aux2->next;
			}
			for(i=1; i<=INT_32 && inicio!=NULL ;i++){  //paso 2=> al terminar de recorrer 32 nodos, estos son eliminados
				temporal=inicio->next;
				free(inicio);
				inicio=temporal;
			}
			U=AddNodoLT(&T,&U,numero); //paso 3=> agregar el numero con sus bit "seteados" a la Lista T
			totalT++;
			ct++;
			if(totalT==1){
				UR=AddNodoToRank(&ListRank,&UR,&U,cuno);
				totalR++;
			}
			if(ct==20){
				UR=AddNodoToRank(&ListRank,&UR,&U,cuno);
				totalR++;
				ct=0;
			}
		}
		
		puts("Pasando L a bits\n");	
		U=NULL;
		UR=NULL;
		cuno=totalT=ct=0;
		MyList aux3=ll->LA;
		while(aux3!=NULL){
			inicio=aux3;
			int numero=0;//todos los bit en 0
			for(i=1; i<=INT_32 && aux3!=NULL ;i++){		//paso 1=> cada 32 nodos creo un entero con sus bits "seteados"	
				if(aux3->var=='1'){
					SET_BIT(numero, (1 << (INT_32-i) ));
				}
				info.taml++;
				aux3=aux3->next;
			}
			for(i=1; i<=INT_32 && inicio!=NULL ;i++){	//paso 2=> al terminar de recorrer 32 nodos, estos son eliminados
				temporal=inicio->next;
				free(inicio);
				inicio=temporal;
			}
			U=AddNodoLT(&L,&U,numero);					//paso 3=> agregar el numero con sus bit "seteados" a la Lista L
			ct++;
			totalT++;
			if(totalT==1){
				UR=AddNodoToRank(&ListRankL,&UR,&U,0*32);
				totalRL++;
			}
			if(ct==20){
				UR=AddNodoToRank(&ListRankL,&UR,&U,totalT*32);
				totalRL++;
				ct=0;
			}
		}
		
		CreateBinary(T,nombre,1);
		CreateBinary(L,nombre,0);
		
		gettimeofday(&t_fin, NULL);
		printf("\nTamaño RANK = %d Tamaño T = %d Tamaño L = %d \n",totalR,info.tamt,info.taml);

		secs = timeval_diff(&t_fin, &t_ini);
		printf("Tiempo de construcción: %.16g ms", secs * 1000.0);

		saveInfo(info,nombre);
		free(T);
		free(L);
	}
}

/**
	@brief Función para aproximar el n a la próxima potencia
	@param n valor n leido desde k2-tree
	@param k valor k entregado por teclado
	@return valor de la próxima potencia (en caso que hubiera)
*/
int ProxPotencia(int n, int k){
	int i,p;
	for(i=0;i<n;i++){
		p=pow(k,i);
		if(p==n)return n;
		if(p>n)return p;
	}
}

/**
	@brief Función recursiva para crear el bitmap
	@param n tamaño de matriz ajustada
	@param max tamaño real de matriz
	@param nivel nivel de profundidad del bitmap
	@param p indice para marcar valor recursivo
	@param q indice para marcar valor recursivo
	@param **bm matriz leida desde archivo
	@param k valor k solicitado por teclado
	@return valor en el nivel
*/
char BitMap(int n,int max,int nivel,int p,int q,char **bm, int k){
	char v[(k*k)];
	int i,j,t,tt=0,ll=0;
	for(i=0;i<k;i++){
		for(j=0;j<k;j++){
			if(n==k){
				if((p+i)>=max ||(q+j)>=max )v[ll]='0';
				else v[ll]=bm[p+i][q+j];
				ll++;	
			}
			else{
				v[tt]=BitMap(n/k,max,nivel+1,p+(i*(n/k)),q+(j*(n/k)),bm,k);
				tt++;
			}
		}
	}
	if(ll!=0){
		for(i=0;i<(k*k);i++){
			if(v[i]=='1'){
				for(t=0;t<(k*k);t++){
					if(v[t]=='1')AddL('1');
					else AddL('0');	
				}
				return '1';				
			}
		}
	}
	for(i=0;i<(k*k);i++){
		if(v[i]=='1'){
			for(t=0;t<(k*k);t++){
				if(v[t]=='1') AddT(nivel,'1');
				else AddT(nivel,'0');
			} 
			return '1';				
		}
	}
	return '0';
}

/**
	@brief Función para agregar valor a L
	@param bit valor a agregar a L
*/
void AddL(char bit){
	ListAdy aux=ll;
	AddNodo(&aux->LA,&aux->ultimo,bit);
}

/**
	@brief Función para agregar valor a T
	@param nivel nivel donde se agregará valor
	@param bit valor a agregar a T
*/
void AddT(int nivel, char bit){
	ListAdy aux=lt;
	while(aux!=NULL){
			if(aux->nivel==nivel){
				AddNodo(&aux->LA,&aux->ultimo,bit);
				return;
			}
			aux=aux->next;
	}
}

/**
	@brief Función que crea los niveles de la escructura T
	@param taman valor n leido desde archivo
	@param k valor k solicitado desde teclado
	@return ListAdy con niveles creados
*/
ListAdy CreateColumnOf(int taman, int k){
	int i;
	ListAdy nodo,inicio;
	for(i=0;taman!=k;i++,taman/=k){
		if(i==0){
			nodo = (ListAdy)malloc(sizeof(superlist));
			MyList l = NULL;
			MyList u = NULL;
			nodo->LA=l;
			nodo->ultimo=u;
			nodo->nivel=i;
			nodo->next=NULL;
			inicio=nodo;		
		}
		else{
			ListAdy nuevo = (ListAdy)malloc(sizeof(superlist));
			MyList l = NULL;
			MyList u = NULL;
			nuevo->ultimo=u;
			nuevo->LA=l;
			nuevo->nivel=i;
			nuevo->next=NULL;
			nodo->next=nuevo;
			nodo=nuevo;
		}
	}
	return inicio;
}
/**
	@brief
	@param *a
	@param *b
	@return
*/
double timeval_diff(struct timeval *a, struct timeval *b){
  return
	(double)(a->tv_sec + (double)a->tv_usec/1000000) -
	(double)(b->tv_sec + (double)b->tv_usec/1000000);
}
/**
	@brief
	@param BM
	@param *nombre
	@param flag
*/
void CreateBinary(MyBM BM, char * nombre, int flag){
	FILE *f;
	char archivo[100];
	strcpy(archivo,nombre);
	if (flag == 1) f = fopen(strcat(archivo,"_T.dat"),"w");
	else f = fopen(strcat(archivo,"_L.dat"),"w");
	
	MyBM aux = BM;

	if (f == NULL){ 
		perror("\nNo se puede abrir archivo.");
		return;
	}
	else{
		while(aux != NULL){
			fwrite(&(aux->var),sizeof(int),1,f);
			aux = aux->next;
		}
	}
	
	fclose(f);
}

/**
	@brief
	@param *nombre
	@param flag
	@param tam
	@return 
*/
MyBM ReadBinary(char * nombre, int flag, int tam){
	FILE *f;
	int i,num;
	char archivo[100];
	strcpy(archivo,nombre);
	MyBM last = NULL, NewBM = NULL;
	printf("Nombre archivo: %s\n", archivo);
	if (flag == 1) f = fopen(strcat(archivo,"_T.dat"),"r");
	else f = fopen(strcat(archivo,"_L.dat"),"r");

	if(f == NULL){
		perror("\nNo se puede abrir el archivo.");
		exit(1);
		return NULL;
	}
	else{
		if(tam%32 == 0) i=1;
		else i=0;
		while(i <= tam/32){
			fread(&num,sizeof(int),1,f);
			last = AddNodoLT(&NewBM,&last,num);
			i++;
		}

	}

	fclose(f);
	return NewBM;
}

/**
	@brief
	@param **bm
	@param *bmap
	@param n
*/
void readFile(char **bm, char *bmap, int n){
	int i,j,k,l;
	for (i = 0; i < n; i++){
		l=0;
		for(j = 0; j < n / 8; j++){
			unsigned char mask = 0x80;
			for (k = 0; k < 8; k++){
				if ((bmap[j+i * n/8] & mask) != 0){
					bm[i][l] = '1';
				}
				else{
					bm[i][l] = '0';
				}
				mask = mask >> 1;
				l++;
			}
		}
	}
}

/**
	@brief
	@param *f
	@return 
*/

int getN(FILE *f){
	char buf[4];
	rewind(f);
	fread(buf,1,4,f);
	rewind(f);
	int *n = &buf;
	return *n;
}

/**
	@brief
	@param info
	@param *name
*/
void saveInfo(INFO info, char * name){
	FILE *fp;
	char str[100];
	strcpy(str,name);
	fp = fopen(strcat(str,".il"),"w+");
	if(fp == NULL){
		perror("\nNo se puede abrir archivo.");
		return;
	}
	else fwrite(&info,sizeof(INFO),1,fp);
	fclose(fp);
}

/**
	@brief
	@param *name
	@return
*/
INFO readInfo(char * name){
	FILE *fp;
	INFO info;
	char str[100];
	strcpy(str,name);
	fp = fopen(strcat(str,".il"),"r");
	if(fp == NULL){
		perror("\nNo se puede abrir archivo.");
		exit(1);
		return;
	}
	else fread(&info,sizeof(INFO),1,fp);
	fclose(fp);
	return info;
}

/**
	@brief
	@param res
	@return
*/
MyBM CreateT(ListAdy res){
	puts("Pasando T a Bits.\n");
	int i,numero;
	MyBM T = NULL, U = NULL;
	MyList aux2 = res->LA;
	MyList inicio,temporal;
	while(aux2!=NULL){
		inicio=aux2;
		numero=0;//todos los bit en 0
		for(i=1; i<=INT_32 && aux2!=NULL ;i++){    //paso 1=> cada 32 nodos creo un entero con sus bits "seteados"					
			if(aux2->var=='1'){
				SET_BIT(numero, (1 << (INT_32-i) ));
			}
			aux2=aux2->next;
		}
		for(i=1; i<=INT_32 && inicio!=NULL ;i++){  //paso 2=> al terminar de recorrer 32 nodos, estos son eliminados
			temporal=inicio->next;
			free(inicio);
			inicio=temporal;
		}
		U=AddNodoLT(&T,&U,numero); //paso 3=> agregar el numero con sus bit "seteados" a la Lista T
	}
	return T;
}

/**
	@brief
	@param res
	@return
*/
MyBM CreateL(ListAdy res){
	puts("Pasando L a bits\n");	
	int i,numero;
	MyBM L=NULL, U=NULL;
	MyList inicio, temporal;

	MyList aux3=res->LA;
	while(aux3!=NULL){
		inicio=aux3;
		numero=0;//todos los bit en 0
		for(i=1; i<=INT_32 && aux3!=NULL ;i++){		//paso 1=> cada 32 nodos creo un entero con sus bits "seteados"	
			if(aux3->var=='1'){
				SET_BIT(numero, (1 << (INT_32-i) ));
			}
			aux3=aux3->next;
		}
		for(i=1; i<=INT_32 && inicio!=NULL ;i++){	//paso 2=> al terminar de recorrer 32 nodos, estos son eliminados
			temporal=inicio->next;
			free(inicio);
			inicio=temporal;
		}
		U=AddNodoLT(&L,&U,numero);					//paso 3=> agregar el numero con sus bit "seteados" a la Lista L
	}
	return L;
}

ListAdy CreateColumnOfC(int taman, int k){
	int i;
	ListAdy nodo,inicio;
	for(i=0;taman!=k;i++,taman/=k){
		if(i==0){
			nodo = (ListAdy)malloc(sizeof(superlist));
			MyList l = NULL;
			MyList u = NULL;
			nodo->LA=l;
			nodo->ultimo=u;
			nodo->nivel=i;
			nodo->next=NULL;
			inicio=nodo;		
		}
		else{
			ListAdy nuevo = (ListAdy)malloc(sizeof(superlist));
			MyList l = NULL;
			MyList u = NULL;
			nuevo->ultimo=u;
			nuevo->LA=l;
			nuevo->nivel=i;
			nuevo->next=NULL;
			nodo->next=nuevo;
			nodo=nuevo;
		}
	}
	ListAdy nuevo = (ListAdy)malloc(sizeof(superlist));
	MyList l = NULL;
	MyList u = NULL;
	nuevo->ultimo=u;
	nuevo->LA=l;
	nuevo->nivel=i;
	nuevo->next=NULL;
	nodo->next=nuevo;
	nodo=nuevo;
	return inicio;
}