/**
	@file bitmap2.c
	@brief Archivo principal
	@author Gonzalo Candia - Tamara Vivanco
*/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include "BIT.h"
#include "List.h"
#include "Rank.h"
#include "Build.h"
#include "Complement.h"
#include "Difference.h"
#include "Intersection.h"
#include "Union.h"

/**
	@brief Función principal
*/
int main(int argc, char const *argv[]){

	int option;
	printf("CONSTRUCCIÓN Y OPERACIONES BINARIAS SOBRE K2-TREE\n");
	do{
		printf("\nElija la acción a realizar\n1) Construir k2-tree\n2) Ver Bitmap\n");
		printf("3) Complemento\n4) Diferencia\n5) Interseccion\n6) Union\n0) Salir\n");
		scanf("%d",&option);
		switch(option){
			case 1: 
				Build();		
				break;
			case 2:
				system("clear");
				char nombre[100];
				printf("\nIngrese el nombre de la estructura a mostrar:\n");
				scanf("%s",nombre);
				INFO info = readInfo(nombre);
				puts("T \n");
				ViewBM(ReadBinary(nombre,1,info.tamt));	
				puts("L \n");
				ViewBM(ReadBinary(nombre,0,info.taml));
				break;
			case 3:
				Complement();
				break;
			case 4:
				Difference();
				break;
			case 5:
				Intersection();
				break;
			case 6:
				Union();
				break;
			case 0:
				printf("Hasta luego!\n");
				break;
			default:
				printf("Opción no permitida.\n");
		}
	}while(option != 0);
	exit(0);
	return 0;
}