/**
	@file Complement.c
	@brief Funciones del algoritmo complemento
	@author Gonzalo Candia - Tamara Vivanco
*/

#include "Complement.h"
#include "Build.h"
#include "List.h"
#include "Rank.h"

/**
	@brief Función para devolver el BM creado
*/
void Complement(){
	struct timeval t_ini, t_fin;
	char nombre[100];
	printf("==== COMPLEMENTO ====\n");
	printf("\nIngrese el nombre de matriz de entrada:\n");
	scanf("%s",nombre);
	INFO tree = readInfo(nombre);
	comp = CreateColumnOfC(tree.pot,tree.k);
	MyBM T = ReadBinary(nombre,1,tree.tamt);
	MyBM L = ReadBinary(nombre,0,tree.taml);
	ListRank = AssociateRankT(&T);
	ListRankL = AssociateRankL(&L);

	gettimeofday(&t_ini, NULL);
	if (Comp(0,0,tree) == 1){
		gettimeofday(&t_fin, NULL); 
		double secs = timeval_diff(&t_fin, &t_ini);
		printf("Hay complemento.\n");
		printf("Tiempo de construcción: %.16g microsegs", secs * 1000000.0);
	}else{
		printf("No se encontró complemento.\n");
	}
	ViewListAdy(comp);
	ListAdy compL = splitListAdy(comp);

	ListAdy aux=comp;
	while(aux!=NULL){
		if(aux->next!=NULL) aux->ultimo->next = aux->next->LA;
		aux=aux->next;
	}

	MyBM resT = CreateT(comp);
	MyBM resL = CreateL(compL);

	char archivo[100];
	strcpy(archivo,nombre);
	CreateBinary(resT,strcat(archivo,"comp"),1);
	strcpy(archivo,nombre);
	CreateBinary(resL,strcat(archivo,"comp"),0);
}


/**
	@brief Función recursiva que devuelve el complemento dado un BM
	@param l nivel del bitmap en el cual se va a buscar
	@param *pA puntero a la posición en la que se desea
	@param tree información del árbol
	@return un 1, si existe complemento o un 0, en caso contrario
*/
int Comp(register int l, register int pA, INFO tree){
	register int i, sw = 0, k2 = tree.k*tree.k, h = tree.height-1;
	int t[k2]; // Variable switch y vector de enteros de tamaño k*k
	for (i = 0; i < k2; i++){
		t[i] = 1;
		// Llamada a globalSearch. Retorna el valor que existe en la posición X, en el bitmap TL.
		if (globalSearch(pA,tree.tamt) == 1){
			if (l < h){
				// Llamada recursiva a la función, con l+1 nivel, y la posición de los hijos del padre.
				t[i] = Comp((l + 1), Child(pA,k2,ListRank), tree);
			}
			else{
				if(t[i] == 0) t[i] = 1;
				else t[i] = 0;
			}
		}else{
			if(l < h)
				FillIn(l + 1,tree); //Si encuentra un 0 interno rellena con 1's hasta las hojas.	
		}
		sw |= t[i];// Cambia el valor a sw de acuerdo al operador binario OR.
		++pA;// Avanza a la siguiente posición del bitmap
	}
	if (sw == 1){ 
		for (i = 0; i < k2; i++){
			if(t[i] == 1) catLevel(comp,l,'1');
			if(t[i] == 0) catLevel(comp,l,'0');
		}
	}
	return sw;
}

/**
	@brief Función que cambiar el valor de la posición del padre al valor de la del primer hijo
	@param x
	@param k2
	@param ListRank
	@return posición del primer hijo
*/
int Child(int x, int k2, Rank ListRank){
	return (ApplyRank(ListRank, x)*k2); 
}

/**
	@brief Función que rellena con 1 recursivamente si se encuentra un 0 en el nivel superior
	@param level nivel en el cual se debe rellenar con 1's
	@param tree información del árbol
*/
void FillIn(register int level, INFO tree){
	register int i, k2 = tree.k*tree.k;
	if (level == (tree.height-1)){
		for(i = 0; i < k2; i++)
			catLevel(comp,level,'1');
	}
	else{
		for(i=0; i < (k2 - 1); i++){
			catLevel(comp,level,'1');
			FillIn(level + 1,tree);
		}
	}
}

/**
	@brief Función que concatena una lista MyList a un nivel de comp dado
	@param comp List Ady por niveles en cual se concatenará la lista
	@param level
	@param bit
*/
void catLevel(ListAdy comp, int level, char bit){
	ListAdy aux=comp;
	while(aux!=NULL){
		if(aux->nivel == level){
			AddNodo(&aux->LA,&aux->ultimo,bit);
			return;
		}
		aux=aux->next;
	}
}

/**
	@brief Función que retorna el valor de una posición del Bitmap (T||L)
	@param pos posición del bitmap para la cual se quiere saber el valor
	@param TAMT
	@return valor del bitmap en la posición dada
*/
int globalSearch (int pos, int TAMT){
	if (pos < TAMT) return newRank(ListRank, pos);
	else return newRank(ListRankL, pos-TAMT);
}