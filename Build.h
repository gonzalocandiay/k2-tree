#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>

#ifndef BUILD_H
#define BUILD_H

typedef struct treeinfo{
	int n;
	int k;
	int tamt;
	int taml;
	int pot;
	int height;
}INFO;

int ProxPotencia(int n, int k);
char BitMap(int n,int max,int nivel,int p,int q,char **bm, int k);
void AddL(char bit);
void AddT(int nivel, char bit);
ListAdy CreateColumnOf(int taman, int k);
double timeval_diff(struct timeval *a, struct timeval *b);
void CreateBinary(MyBM BM, char * nombre, int flag);
MyBM ReadBinary(char * nombre, int flag, int tam);
void readFile(char **bm, char *bmap, int n);
int getN(FILE *f);
INFO readInfo(char * name);
void saveInfo(INFO info, char * name);
MyBM CreateT(ListAdy res);
MyBM CreateL(ListAdy res);
ListAdy CreateColumnOfC(int taman, int k);

#include"Build.c"
#endif /*BUILD_H*/