#include <stdio.h>
#include <stdlib.h>

#ifndef INTERSECTION_H
#define INTERSECTION_H

ListAdy inter = NULL;
Rank ListRankIA = NULL;
Rank ListRankILA = NULL;
Rank ListRankIB = NULL;
Rank ListRankILB = NULL;
int Inter(int l, int pA, int pB, INFO treeA, INFO treeB);
int globalSearchInterA (int pos, int TAMT);
int globalSearchInterB (int pos, int TAMT);

#include "Intersection.c"
#endif