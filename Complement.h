#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#ifndef COMPLEMENT_H
#define COMPLEMENT_H

Rank ListRank = NULL;
Rank ListRankL = NULL;
ListAdy comp = NULL;

int Comp(register int l, register int pA, INFO tree);
int Child(int x, int k2, Rank ListRank);
void FillIn(register int level, INFO tree);
void catLevel(ListAdy comp, int level, register char bit);
int globalSearch (int pos, int TAMT);


#include "Complement.c"
#endif